package com.oreilly.start.testen

import sun.plugin2.message.Message

sealed trait Message
case class Draw(shape: String) extends Message
case class Response(message: String) extends Message
case object Exit extends Message
