package com.oreilly.start.testen

class Point(val x: Double, val y: Double) {
}

object Point{
  def apply(x: Double = 0.0, y: Double = 0.0) = new Point(x, y)
}
