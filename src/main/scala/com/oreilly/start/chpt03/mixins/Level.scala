package com.oreilly.start.chpt03.mixins

enum Level {
  case Trace, Info, Warn, Error

  def ==(other: Level): Boolean = this.ordinal == other.ordinal

  def >=(other: Level): Boolean = this.ordinal >= other.ordinal
}

trait Logging {

  import Level.*

  def level: Level

  def log(level: Level, message: String): Unit

  final def info(message: String): Unit = if (level >= Info) {
    log(Info, message)
  }

  final def warn(message: String): Unit = if (level >= Warn) {
    log(Warn, message)
  }

  final def error(message: String): Unit = if (level >= Error) {
    log(Error, message)
  }
}

trait StdOutLogging extends Logging {
  override def log(level: Level, message: String): Unit = println(s"${level.toString}: $message")
}
