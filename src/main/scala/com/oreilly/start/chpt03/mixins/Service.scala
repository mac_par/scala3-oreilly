package com.oreilly.start.chpt03.mixins

import scala.util.Random

open class Service(name: String) {
  def work(i: Int): (Int, Int) = (i, Random.between(0, 1000))
}
