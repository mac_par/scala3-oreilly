package com.oreilly.start.chpt03.mixins

case class LoggingService(name: String, level: Level) extends Service(name) with StdOutLogging {
  override def work(i: Int): (Int, Int) = {
    info(s"Starting i=$i")
    val result = super.work(i)
    info(s"Ending work with result: $result")
    result
  }
}
