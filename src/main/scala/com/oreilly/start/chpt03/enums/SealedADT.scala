package com.oreilly.start.chpt03.enums

object SealedADT {
  sealed trait Tree[T]

  final case class Branch[T](left: Tree[T], right: Tree[T]) extends Tree[T]

  final case class Leaf[T](elem: T) extends Tree[T]
}
