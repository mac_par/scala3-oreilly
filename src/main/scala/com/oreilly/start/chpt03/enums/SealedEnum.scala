package com.oreilly.start.chpt03.enums

object SealedEnum {
  enum Tree[T] {
    case Branch(left: Tree[T], right: Tree[T]) extends Tree[T]
    case Leaf[T](elem: T) extends Tree[T]
  }
}
