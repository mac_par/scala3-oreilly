package com.oreilly.start.chpt03.enums

class WeekDays {

}

enum WeekDaySimple {
  case Sun, Mon, Tue,Wed, Thu, Fri, Sat
}

enum WeekDay(val fullName: String, val work: Boolean = true) {
  case Sun extends WeekDay("Sunday", false)
  case Mon extends WeekDay("Monday")
  case Tue extends WeekDay("Tuesday")
  case Wed extends WeekDay("Wednesday")
  case Thu extends WeekDay("Thursday")
  case Fri extends WeekDay("Friday")
  case Sat extends WeekDay("Saturday", true)
}
