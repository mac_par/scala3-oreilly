package com.oreilly.start.chpt03.formates

object StrFormat extends App {
  val formatka = "%d, %02.2f, %s"
  println(formatka.format(1, 2.15, "dupa"))

  val noEscape = "there\nis\nno\nescape from\t%s"
  println(noEscape.format("dupa"))
  val sthValue ="dupa"
  val rawEscape = raw"there\nis\nno\nescape from\t${sthValue}"
  println(rawEscape)
}
