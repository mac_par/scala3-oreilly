package com.oreilly.start.chpt03.operators

case class InfixTries(s: String) {
  def append(s: String): InfixTries = copy(this.s + s)
  infix def combine(s: String):InfixTries = append(s)


}
