package com.oreilly.start.chpt03.operators

object InfixObject extends App {

  println(InfixTries("one").append("two"))
  println(InfixTries("one") append {
    "two"
  })
  println(InfixTries("one") `append` "two")
  println(InfixTries("one") append "two")
  println(InfixTries("one") combine "two")
}
