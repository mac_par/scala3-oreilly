package com.oreilly.start.chpt03.operators

import java.io.File
import scala.annotation.targetName

case class PathGen(value: String, separator: String = PathGen.defaultSeparator) {

  override def toString: String = value

  @targetName("concat") def / (node: String): PathGen = copy(value + separator + node)

  infix def append(node: String): PathGen = /(node)
}

object PathGen {
  val defaultSeparator = sys.props("file.separator")
}
