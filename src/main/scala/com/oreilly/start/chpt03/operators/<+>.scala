package com.oreilly.start.chpt03.operators

import scala.annotation.targetName

@targetName("TIEFighter") infix case class <+>[A,B](a:A, b: B)
