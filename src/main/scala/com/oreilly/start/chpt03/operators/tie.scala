package com.oreilly.start.chpt03.operators

infix case class tie[A, B](a: A, b: B)

object tie extends App {
//  val t1: String tie Int = "two" tie 1
  val t2: String tie Int = tie("two", 1)

//  println(t1)
  println(t2)

//  val fighter1:  Int <+> String = 1 <+> "one"
  val fighter2:  Int <+> String = <+>(1,"one")
}
