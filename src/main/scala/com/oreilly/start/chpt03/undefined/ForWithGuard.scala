package com.oreilly.start.chpt03.undefined

import com.oreilly.start.chpt03.enums.WeekDay

object ForWithGuard extends App {
  for n <- 0 until 6 if n % 2 == 0 do println(n)

  val zawjiyya = for n <- 0 to 13 if n % 2 == 0 yield n
  println(zawjiyya)

  val days = for day <- WeekDay.values if (day.work)
                 fn = day.fullName
  yield fn

  println(days.toVector)

  import WeekDay.*

  val seqOpt = Seq(Some(Mon), Some(Fri), None, Some(Wed))

  val goodDays = for {dayOpt <- seqOpt
                      day <- dayOpt
                      fn = day.fullName} yield fn
  println(goodDays.toSeq)
}
