package com.oreilly.start.chpt03.undefined

object Sth01 extends App {
  (0 until 6) map {n =>
    if (n %2 == 0) {
      s"$n is divisible by 2"
    } else if (n % 3 == 0) {
      s"$n is divisable by 3"
    } else {
      n.toString
    }
  } foreach(println)

  (0 until 6) map {n =>
    if n %2 == 0 then
      s"$n is divisible by 2"
    else if n % 3 == 0 then
      s"$n is divisable by 3"
    else
      n.toString
  } foreach(println)

  (0 until 6) map { n =>
    if n % 2 == 0 then Some(s"$n divisable by 2")
    else None
  } foreach(println)
}
