package com.oreilly.start.chpt03.undefined

object NullenFun extends App {
  val s: String | Null = null

  println(s == null)

  for {i <- (0 to 6)}{
    println(i)
  }

  for i<- 0 to 6 do println(i)

  for {(key, value) <- Map(1->"tak", 2-> "nie")} {
    println(s"$key:$value")
  }
}
