package com.oreilly.start.chpt03.matching

object CaseMatching extends App {
  val alice = Person("Alice", 25, Address("1 Scala Lane", "Chicago"))
  val bob = Person("Bob", 29, Address("2 Java Ave.", "Miami"))
  val charlie = Person("Charlie", 32, Address("3 Python Lane", "Klewki"))

  val results = Seq(alice, bob, charlie) map {
    case p @ Person("Alice", age, a @ Address(_, "Chicago")) => s"Hi Alice! $p"
    case p @ Person("Bob", 29, a @ Address(street, city)) => s"Hi ${p.name}, age: ${p.age}, in $a"
    case p @ Person(name, age, Address(street, city)) => s"Who are you, $name($age, city = $city)?"
  }

  println(results)

  println(Seq((1, "t"), (2, "n")) map {case p @ (x,y) => s"$p"})

  val seq = Seq(("Pencil", 3.15), ("Stabler", 2.99), ("Scissors", 1.9))
  println(seq.zipWithIndex  map {
    case ((item, price), idx) => s"$item for $price at $idx"
  })
}

case class Address(street: String, city: String)
case class Person(name: String, age: Int, address: Address)
