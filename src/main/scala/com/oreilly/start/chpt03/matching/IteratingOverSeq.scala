package com.oreilly.start.chpt03.matching

object IteratingOverSeq extends App {
  val seq: Seq[Int] = (1 to 10).toList

  seq match {
    case head +: tail => println(s"$head : $tail")
    case Nil => println("empty list")
  }

  seq match {
    case Seq(head, second, rest*) => println(s"$head : $second : $rest")
    case Nil => println("empty list")
  }

  val list = ((1, "one") +: ((2, "two")) +: ((3, "three") +: Nil))
  val map = Map(list *)
  println(map)
  println(map map {
    case i *: j => s"$i <-> $j"
  })
  println(EmptyTuple.toString)
}
