package com.oreilly.start.chpt03.matching

object MatchingWithGuard extends App {
  val res = Seq(1,2,3,4) map {
    case v: Int if v % 2 == 0 => s"even: $v"
    case v => s"odd: $v"
  }

  println(res)
}
