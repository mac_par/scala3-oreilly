package com.oreilly.start.chpt03.trycatch

import scala.io.Source
import scala.util.control.NonFatal

object TryCatchTry01 extends App {
  def tryToRead(filename: String):Unit = {
    var source: Option[Source] = None
    try {
      source = Some(Source.fromFile(filename))
      val size = source.get.getLines().size
      println(s"file $filename size: $size")
    } catch {
      case NonFatal(ex) => println(s"Error: ${ex.getMessage}")
    } finally {
      for (s <- source) {
        println(s"closing file: $filename")
        s.close()
      }
    }
  }

  tryToRead("dupa.txt")
}
