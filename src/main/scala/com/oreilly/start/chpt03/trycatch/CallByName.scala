package com.oreilly.start.chpt03.trycatch

import scala.annotation.tailrec

object CallByName extends App {
  @tailrec
  def continue(condition: => Boolean)(body: => Unit): Unit = {
    if (condition) {
      body
      continue(condition)(body)
    }
  }
// type erasure
//  def continue(condition: => Int)(body: => Unit): Unit = {}

  var i = 0
  continue {
    i < 5
  } {
    i += 1
    println(s"going thurder: $i")
  }
}
