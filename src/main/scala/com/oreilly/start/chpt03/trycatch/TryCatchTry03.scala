package com.oreilly.start.chpt03.trycatch

import scala.io.Source
import scala.util.Using
import scala.util.control.NonFatal
import reflect.Selectable.reflectiveSelectable

object TryCatchTry03 extends App {
  def apply[R <: {def close(): Unit}, T](r: => R)(f: R => T): T = {
    var res: Option[R] = None
    try {
      println("attempt for resource")
      res = Some(r)
      f(res.get)
    } catch {
      case NonFatal(ex) => println(s"manage.apply: Non fatal exception! $ex")
        throw ex
    } finally {
      res match {
        case Some(resource) => resource.close()
        case None => //
      }
    }
  }

//  apply(Source.fromFile("filename")) { source => source.getLines().size }
  println(apply{println("get resource")
    Source.fromFile("tekst.txt")} { source => println("closing file");source.getLines().size })
}
