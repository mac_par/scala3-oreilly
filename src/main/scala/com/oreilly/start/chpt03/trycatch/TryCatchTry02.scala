package com.oreilly.start.chpt03.trycatch

import scala.io.Source
import scala.util.Using
import scala.util.control.NonFatal

object TryCatchTry02 extends App {
  def tryToRead(filename: String): Unit = {
    try {
      Using.resource(Source.fromFile(filename)) { source =>
        val size = source.getLines().size
        println(s"file $filename size: $size")
      }
    } catch {
      case NonFatal(ex) => println(s"Error: ${ex.getMessage}")
    }
  }

  tryToRead("dupa.txt")
}
