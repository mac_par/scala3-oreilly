package com.oreilly.start.chpt03.methods

object Tryout1 extends App {
  val seq = Seq(1)
  seq.size

  val seqa = Seq('A', 'B', 'C')
  val d: Char = 'D'
  val seqd = d +: seqa
  println(seqd)
  val seqd2 = seqa.+:(d)
  println(seqd2)
}
