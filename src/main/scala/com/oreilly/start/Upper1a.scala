package com.oreilly.start

class Upper1a:
  def convert(strings: Seq[String] = Seq()): Seq[String] = strings.map(_.toUpperCase)
