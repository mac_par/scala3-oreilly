package com.oreilly.start

object UberMain {
  @main
  def hi(args: String*): Unit = {
    args.foreach(println)
  }
}
