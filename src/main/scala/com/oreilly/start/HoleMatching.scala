package com.oreilly.start


object HoleMatching extends App {
  def examine[T](seq:Seq[T]):Seq[String] = seq map {
    case i: Int => s"Int: $i"
    case other => s"Other: $other"
  }

  val array = Array(1, 2, 3, 4, 5)
  array match {
    case arr: Array[Int] => arr(2) = 300
  }

  println(array.mkString(","))

  println(examine(Seq(1,"dwa", 3.0)))
}
