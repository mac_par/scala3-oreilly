package com.oreilly.start.chpt02.optional

import java.util.{List => _}
import java.util.HashMap as JHashMap

object HashMapenStyle extends App {
  val jhm = JHashMap[String,String]()
  jhm.put("one", "1")

  val one: String = jhm.get("one")
  val one2: String | Null = jhm.get("one")

  val two1: String = jhm.get("two")
  val two2: String | Null = jhm.get("two")


  println(s"o1: $one, o2: $one2, t1: $two1, t2: $two2")
}
