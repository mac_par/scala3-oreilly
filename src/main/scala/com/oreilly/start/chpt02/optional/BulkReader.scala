package com.oreilly.start.chpt02.optional

import scala.io.Source

abstract class BulkReader {
  type In
  val source: In

  def read: Seq[String]
}

case class StringBulkReader(source: String) extends BulkReader {
  override type In = String

  override def read: Seq[String] = Seq(source)
}

case class FileBulkReader(source: Source) extends BulkReader {
  override type In = Source

  override def read: Seq[String] = source.getLines().toSeq
}
