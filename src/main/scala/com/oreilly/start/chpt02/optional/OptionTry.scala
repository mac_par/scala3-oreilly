package com.oreilly.start.chpt02.optional

object OptionTry extends App {
  val map = Map("a" -> "b", "b"->"c", "d"->"e")
  val a = map.get("a")
  val b = map.getOrElse("c", "default")
  val c = map.get("c")

  println(s"a: $a,b: $b, c: $c")
  map("a")
  map("c")
}
