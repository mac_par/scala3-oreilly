package com.oreilly.start.chpt02.optional

import scala.io.Source

abstract class BulkReader2[T](source: T) {
  def read: Seq[String]
}

case class StringBulkReader2(source: String) extends BulkReader2[String](source) {
  override def read: Seq[String] = Seq(source)
}

case class FileBulkReader2(source: Source) extends BulkReader2[Source](source) {
  override def read: Seq[String] = source.getLines().toSeq
}
