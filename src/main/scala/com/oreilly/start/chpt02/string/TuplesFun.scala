package com.oreilly.start.chpt02.string

object TuplesFun extends App {
  val i = 10
  val s = "Dupa"
  val d = 3.14D
  val mytuple = (i,s,d)
  val (int,string, doubleVal) = mytuple

  println(s"$int, $string, $doubleVal")
  println(s"${mytuple(0)}, ${mytuple(1)}, ${mytuple(2)}")
  println(mytuple.productIterator.mkString(","))

  val myTuple = 1 -> "one"
  val (nbrm, stringen) = myTuple
  println(s"$nbrm, $stringen")
}
