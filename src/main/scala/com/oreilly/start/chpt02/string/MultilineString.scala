package com.oreilly.start.chpt02.string

object MultilineString extends App {
  val welcome = s"""Welcome!
       |  Hello!
       |  *(Gratuitous Star character!!)
       |  |This line has a margin indicator.
       |  | This line has some extra whitespace.""".stripMargin
    println(welcome)

    val tryNbr2 = "<hello> <and> <world>".stripPrefix("<").stripSuffix(">")
    println(tryNbr2)
}
