package com.oreilly.chpt02.newsyntax

object NewSyntax extends App {
  var i = 1
  if i > 0 then
    println("new syntax")
  end if

  while i < 10 do
    i = i + 1
    println("incrementacja")
  end while

  for j <- 0 to 10 do
    println(s"For with ${j}")
//  end for is optional
}
