package com.oreilly.start.chpt02.newsyntax

object ImmutabilityShow extends App {
  //Seq is immutable
  val seq = Seq(5, 10, 15)
  seq.reduce((x, y) => x + y)
  println(seq)
  //  Array is mutable
  val arr = Array("This", "is", "Sparta", "!")
  println(arr.mkString("[", ",", "]"))
  arr(2) = "dupa"
  println(arr.mkString("[", ",", "]"))
}
