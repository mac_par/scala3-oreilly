package com.oreilly.start.chpt02.nested

import scala.annotation.tailrec

object Nesting01 extends App {
  def factorial(i: Int): BigInt = {
    @tailrec
    def factorialAccumulator(i: Int, acc: BigInt): BigInt = {
      if (i <= 1) {
        acc
      } else {
        factorialAccumulator(i - 1, i * acc)
      }
    }

    factorialAccumulator(i, BigInt(1))
  }

  def fibonacci(i: Int): BigInt = {
    if (i == 1) {
      BigInt(1)
    } else if (i <= 0) {
      BigInt(0)
    } else {
      fibonacci(i - 1) + fibonacci(i - 2)
    }
  }

  def counter(n: Int): Unit = {
    @tailrec
    def count(i: Int): Unit = {
      if (i <= n) {
        println(i)
        count(i + 1)
      }
    }

    println(s"Counting to $n")
    count(1)
  }

  (0 to 5) foreach { i =>
    println(s"factorial: $i -> ${factorial(i)}")
    println(s"fibonacci: $i -> ${fibonacci(i)}")
  }

  counter(10)
}
