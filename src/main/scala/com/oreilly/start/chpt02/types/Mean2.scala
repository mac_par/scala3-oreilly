package com.oreilly.start.chpt02.types

object Mean2 {
  def apply(d: Double, ds: Double*): Double = apply(d +: ds)

  def apply(ds: Seq[Double]): Double = ds.sum / ds.size
}
