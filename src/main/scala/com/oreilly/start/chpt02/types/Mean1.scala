package com.oreilly.start.chpt02.types

object Mean1 {
  def calc1a(ds: Double*): Double = calc1b(ds)

  def calc1b(ds: Seq[Double]): Double = ds.sum / ds.size

  def calc2a(ds: Double*): Double = ds.sum / ds.size

  def calc2b(ds: Seq[Double]): Double = calc2a(ds *)
}
