package com.oreilly.start.chpt02.types

case class Money(value: BigDecimal)

case object Money {
  def apply(s: String): Money = Money(BigDecimal(s))
  def apply(d: Double): Money = Money(BigDecimal(d))
}
