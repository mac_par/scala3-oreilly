package com.oreilly.start.chpt02.methods

import scala.concurrent.{ExecutionContext, ExecutionContextExecutor, Future}
import scala.util.{Success, Failure}

object ConcurrentFun extends App {

  given executionContext: ExecutionContextExecutor = ExecutionContext.global
  def sleep(millis: Long): Unit = Thread.sleep(millis)

  (1 to 5) foreach { i => {
    val future = Future {
      val duration = (math.random() * 1000).toLong
      sleep(duration)
      if (i == 3) {
        throw new RuntimeException(s" $i -> $duration")
      }
      duration
    }

    future.onComplete{
      case Success(value) => println(s"Success: $i => $value")
      case Failure(exception) => println(s"Failure: $i => ${exception}")
    }
  }

    sleep(5000)
  }
}
