package com.oreilly.start.chpt02.methods

case class Point(x: Double, y: Double) {
  def shift(deltaX: Double = 0.0, deltaY: Double = 0.0): Point = copy(x + deltaX, y + deltaY)
}
