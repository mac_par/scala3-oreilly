package com.oreilly.start.chpt02.ranges

object VariousRanges extends App {
 var range1 = Range(1,10)
 var range2 = 1 to 9 by 2
 var range3 = 1 until 9 by 2
 println(range2.mkString(","))
 println(range3.mkString(","))

 var longRange = 1L to 10L
 println(longRange.mkString(","))

 var bigIntRange = BigInt(1) to BigInt(10) by BigInt(2)
 var bigDecimalRange = BigDecimal(1.0) to BigDecimal(10.0) by BigDecimal(2.0)
 println(bigIntRange.mkString(";"))
 println(bigDecimalRange.mkString(":"))
}
