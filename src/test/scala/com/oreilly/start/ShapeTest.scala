package com.oreilly.start

import org.scalatest.funspec.AnyFunSpec

class ShapeTest extends AnyFunSpec {
  describe("Point class") {
    it("A point is created") {
      val x = 12.3
      val y = 3.14
      val point = Point(x, y)
      assert(point.x == x)
      assert(point.y == y)
      assertDoesNotCompile("point.x = 15.3")
    }

    it("only a single parameter is passed, then second is ppopulated with default") {
      val x = 12.3
      val y = 0
      val point = Point(x)
      assert(point.x == x)
      assert(point.y == y)
    }
  }
}