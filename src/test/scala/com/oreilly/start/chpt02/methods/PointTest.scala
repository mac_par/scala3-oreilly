package com.oreilly.start.chpt02.methods

import org.scalatest.funspec.AnyFunSpec

class PointTest extends AnyFunSpec {
  describe("Testing new Point impl") {
    it("using copy method in case class") {
      val x = 5.2
      val y = 3
      val newX = 5
      val p1 = Point(x, y)
      val p2 = p1.copy(x = newX)

      assert(p2.x == newX)
      assert(p2.y == p1.y)
    }

    it("using shift method new point X is created") {
      val x = 5.2d
      val y = 3d
      val shiftX = 1.2d
      val p1 = Point(x, y)
      val p2 = p1.shift(deltaX = shiftX)

      assert(p2.x == shiftX + p1.x)
      assert(p2.y == p1.y)
    }

    it("using shift method new point Y is created") {
      val x = 5.2d
      val y = 3d
      val shiftY = 1.2d
      val p1 = Point(x, y)
      val p2 = p1.shift(deltaY = shiftY)

      assert(p2.y == shiftY + p1.y)
      assert(p2.x == p1.x)
    }
  }
}
