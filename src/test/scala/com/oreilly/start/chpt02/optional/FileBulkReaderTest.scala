package com.oreilly.start.chpt02.optional

import org.scalatest.funspec.AnyFunSpec

class FileBulkReaderTest extends AnyFunSpec {
  describe("Testing") {
    it("string should be returned in Seq") {
      val sbr2 = StringBulkReader2("dupa")
      val expectation = Seq("dupa")
      assert(expectation == sbr2.read)
    }
  }
}
