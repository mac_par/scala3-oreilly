package com.oreilly.start.chpt03.mixins

import org.scalatest.funspec.AnyFunSpec

class ServiceTest extends AnyFunSpec {
  describe("testing class") {
    it("printing results") {
      val service = Service("cos")
      (1 to 3) foreach{i => println(s"$i -> ${service.work(i)}")}
    }

    it("printing results with traits: Info") {
      val service = LoggingService("cos", Level.Info)
      (1 to 3) foreach{i => println(s"$i -> ${service.work(i)}")}
    }

    it("printing results with traits: Error") {
      val service = LoggingService("cos", Level.Error)
      (1 to 3) foreach{i => println(s"$i -> ${service.work(i)}")}
    }

    it("printing results with traits: Trace") {
      val service = LoggingService("cos", Level.Trace)
      (1 to 3) foreach{i => println(s"$i -> ${service.work(i)}")}
    }
  }
}
