package com.oreilly.start.chpt03.enums

import org.scalatest.funspec.AnyFunSpec

class WeekDayTest extends AnyFunSpec {
  describe("days of week") {
    it("it should work") {
      val daysSeq = WeekDay.values.sortBy(_.ordinal).toSeq
      val expectation = Seq(WeekDay.Sun, WeekDay.Mon, WeekDay.Tue, WeekDay.Wed, WeekDay.Thu, WeekDay.Fri, WeekDay.Sat)
      assert(expectation == daysSeq)
    }

    it("keeps value") {
      val day = WeekDay.Sun
      assert(0 == day.ordinal)
      assert("Sunday" == day.fullName)
    }
  }
}
