package com.oreilly.start.chpt03.operators

import org.scalatest.funspec.AnyFunSpec

class PathGenTest extends AnyFunSpec {
  describe("testowanie") {
    it("value should be appendend with default separator") {
      val f1 = PathGen("value1")
      val f2 = f1 / "cos"

      assert(f2.toString == "value1/cos")
    }
  }
}
