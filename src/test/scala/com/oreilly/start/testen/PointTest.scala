package com.oreilly.start.testen

import org.scalatest.funspec.AnyFunSpec

class PointTest extends AnyFunSpec {
  describe("Point class") {
    it("A point is created") {
      val x = 12.3
      val y = 3.14
      val point = Point(x, y)
      assert(point.x == x)
      assert(point.y == y)
    }

    it("only a single parameter is passed, then second is ppopulated with default") {
      val x = 12.3
      val y = 0
      val point = Point(x)
      assert(point.x == x)
      assert(point.y == y)
    }
  }

  describe("Testing messages") {
    it("Draw message retrieves its shape") {
      val shape = "Daira"
      val message = Draw(shape)

      assert(shape == getMessageValue(message))
    }

    it("Response message returns its message") {
      val mesg = "Hi There!"
      val message = Response(mesg)

      assert(mesg == getMessageValue(message))
    }

    it("On exit, exit happens") {
      val expected = "exit"
      val message = Exit

      assert(expected == getMessageValue(message))
    }
  }

  def getMessageValue(msg: Message): String = msg match {
    case Draw(arg) => arg
    case Exit => "exit"
    case Response(msg) => msg
  }
}
